# Configuring Keycloak using Ansible

In this workshop you might have already seen a few of the ways you are able to manage clients  in Keycloak:

* Manually using the UI
* Using the export/import function with a JSON file (however this will do more than just your clients)
* Using the REST API of Keycloak

In this part of the workshop we are showing another way to work with the [REST interface of Keycloak](https://www.keycloak.org/docs-api/24.0.4/rest-api/#_overview). This includes APIs to manage users, roles, clients, identity providers and more.
These APIs could be used by your pipeline to update/manage for example the keycloak clients. This way you won't have to interact manually with Keycloak settings, and you are only updating one thing at a time when going to production, everything is automated and (probably) tested.

Of course, you could create something to call the API yourself (as we did in [advanced-w4-rest-crud](advanced-w4-rest-crud.md)), or use the publicly available ansible playbook that uses these APIs. We will now look at how to do the latter.

## Manage the frontend client to keycloak using the ansible-playbook

Requirements:

* <u>**_A UNIX-like machine (such as Ubuntu, macOS) or Windows with [WSL](https://learn.microsoft.com/en-us/windows/wsl/about) installed._**</u>
* pip (Installed by installing [python](https://www.python.org/downloads/))
* ansible (see [Install ansible using pip](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#pip-install))

### For WSL users:
On windows, create a file named `.wslconfig` in `C:/Users<your user name>` with content:
```
[wsl2]
networkingMode=mirrored
```
before (re)starting WSL. Otherwise you cannot access the address where Keycloak is running.

Checkout the repo for the ansible playbooks: https://bitbucket.org/first8/example-keycloak-ansible-playbook/src/master/

A playbook that should create the frontend client is present in the file `clients/frontend.yml`, but half of its fields have empty values. 
Fill in the fields based on [this documentation](https://docs.ansible.com/ansible/latest/collections/community/general/keycloak_client_module.html) and the configuration of the `frontend` client you already have in the Keycloak admin console (which also includes explanations when you hover over the question mark symbol next to a field) . 
You have to fill in all the fields ***except two*** which you should uncomment because they're not applicable for our specific frontend client (hint: the frontend runs on the end-user's browser.)

The solution can be found in `clients/solution.yml`

Make sure your keycloak server is running. If you want to compare the ansible `frontend` client's configuration to the one already in your Keycloak, rename the existing client's Client-ID to something else like `frontendOG` before running the playbook.
Otherwise, the playbook will just update your existing client.

You can run a playbook with this command:

`ansible-playbook {path}/{file}.yml -e auth_username='{your_username}' -e auth_password='{your_password}'`

So just fill in the correct path, file, username, and password. For example:

`ansible-playbook clients/frontend.yml -e auth_username='admin' -e auth_password='admin'`

## Manage the realm settings using the ansible-playbook

Next, we want to increase our access token lifespan from 5 minutes to 20 minutes. Update `realms/workshop.yml`. Use [this documentation](https://docs.ansible.com/ansible/latest/collections/community/general/keycloak_realm_module.html) 
to figure out what field you should add to the playbook (hint: the value is in seconds.) You could of course include other fields as well if you'd like. 

Run the playbook to update your realm. Verify the changed access token lifespan in the Keycloak admin console. (Select the `workshop` realm and inspect `Realm settings`->`Tokens` -> `Access Token Lifespan`).

## Manage the user-federation using the ansible-playbook

There is also a `user-federation` folder. User federations can also be managed using the Keycloak REST API and there is an ansible-playbook present.
Basically, this configures the same LDAP as in [admin-w3-ldap](../admin/admin-w3-ldap.md), except with some fancy additional settings such as periodic synchronization between your glauth ldap server and Keycloak.
Refer to [this documentation](https://docs.ansible.com/ansible/latest/collections/community/general/keycloak_user_federation_module.html) to learn more about what is happening.

Run the playbook to add the user federation. You should be able to see it added in the Keycloak admin console by going to `User Federation`.

![image](images/federation-added.png)

Again, you can hover over the question marks next to fields to learn more about their meaning.

<br>

**You have finished the advanced track of the workshop!**

If you are interested in the other tracks, check out the [README](../README.md), or jump straight to the start of another track (w0 and w1 can be skipped):

* [basic-w2: Oauth2 Glossary](../basic/basic-w2-oath2-glossary.md)
* [admin-w2: Third parties](../admin/admin-w2-third-parties.md)