# Write and install plugins

In this advanced track we will dive deeper into Keycloak. A lot of functionalities are readily available out of the box, 
but what if you want to change the functionality, or altogether add some new functionalities?

Luckily Keycloak can also help you with this. To add or change a functionality, you have to inject a plugin.

## Installing a plugin
Let’s start with the easy part: installing a plugin. For this you need the jar file of the plugin (we will discuss later 
how to write it). Get the ``event-listener-25.0.1`` jar file from the advanced folder of this repository and copy it into the `providers` directory of Keycloak. Loading it is as simple as starting up Keycloak:

* For development mode (use this command for the workshop):
    * Windows: ``bin\kc.bat start-dev``
    * Linux: ``bin/kc.sh start-dev``

You should see in the terminal that Keycloak is detecting a plugin and 
that it is being started:

![image](images/example-event-listener.png)

In the next exercise we will show you how to configure the event listener in Keycloak, so that it will actually print the events.

### NICE TO KNOW:
What if Keycloak is already running? Unfortunately since Keycloak switched to Quarkus there is no possibility to hot 
re-load plugins anymore. This means you have to restart Keycloak to see the changes in the plugins. When starting 
Keycloak it will automatically run the following command:<br>
``bin/kc.[sh|bat] build``<br>
This command will update the configuration and the custom providers, and optimizes your setup.

## Creating a plugin
*You need to have installed [Maven](https://maven.apache.org/download.cgi) to do the rest of the assignment, and have configured it to use our Java 21 JDK. If you want to see a solution to this assignment, or skip it, you can use the repository that generated the jar we installed earlier, as it contains a maven wrapper: [example-event-listener](https://bitbucket.org/first8/example-event-listener/src/master/).*

We now know how to install a plugin. Let’s take a step back and see how to create a plugin.

We will create the event listener that we also mentioned above.

To create the event listener we will need a new Maven project with JDK 21. In the `pom.xml`, make sure the following `<properties>`, `<dependencies>`, and `<build>` are present:

```
<properties>
    <maven.compiler.source>21</maven.compiler.source>
    <maven.compiler.target>21</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <keycloak.version>25.0.1</keycloak.version>
</properties>

<dependencies>
    <dependency>
        <groupId>org.keycloak</groupId>
        <artifactId>keycloak-server-spi</artifactId>
        <version>${keycloak.version}</version>
        <scope>provided</scope>
    </dependency>
    <dependency>
        <groupId>org.keycloak</groupId>
        <artifactId>keycloak-server-spi-private</artifactId>
        <version>${keycloak.version}</version>
        <scope>provided</scope>
    </dependency>
    <dependency>
        <groupId>org.keycloak</groupId>
        <artifactId>keycloak-services</artifactId>
        <version>${keycloak.version}</version>
        <scope>provided</scope>
    </dependency>
</dependencies>
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <version>3.4.1</version>
        </plugin>
    </plugins>
</build>
```
Other than that, the `<name>`, `<groupId>` and `<artifactId>` at the top of the file is entirely up to you.

To create the plugin we need 3 files:

*	The event listener
*	A factory that creates the event listener
*	A config file that tells Keycloak that it should use the newly created factory

#### The event listener
Let’s start with the event listener. The trick here is that Keycloak already provides us with an interface that we only 
have to implement.
Create a class and give it a nice name. This can be whatever you want. However make sure it implements 
EventListenerProvider.

Let's override all the needed methods (both onEvent methods and the close() method)

To keep it simple, we will simply print all the events in the terminal.
To print the regular events, you can use the following codesnippet:
````
private String toString(Event event) {

    StringBuilder sb = new StringBuilder();
    sb.append("type=");
    sb.append(event.getType());
    sb.append(", realmId=");
    sb.append(event.getRealmId());
    sb.append(", clientId=");
    sb.append(event.getClientId());
    sb.append(", userId=");
    sb.append(event.getUserId());
    sb.append(", ipAddress=");
    sb.append(event.getIpAddress());

    if (event.getError() != null) {
        sb.append(", error=");
        sb.append(event.getError());
    }

    if (event.getDetails() != null) {
        for (Map.Entry<String, String> e : event.getDetails().entrySet()) {
            sb.append(", ");
            sb.append(e.getKey());
            if (e.getValue() == null || e.getValue().indexOf(' ') == -1) {
                sb.append("=");
                sb.append(e.getValue());
            } else {
                sb.append("='");
                sb.append(e.getValue());
                sb.append("'");
            }
        }
    }

    return sb.toString();

}
````
Then call this method in the onEvent for the regular events and print it out in the terminal.

For the admin events you can use the following:
````
private String toString(AdminEvent adminEvent) {

    StringBuilder sb = new StringBuilder();
    sb.append("operationType=");
    sb.append(adminEvent.getOperationType());
    sb.append(", realmId=");
    sb.append(adminEvent.getAuthDetails().getRealmId());
    sb.append(", clientId=");
    sb.append(adminEvent.getAuthDetails().getClientId());
    sb.append(", userId=");
    sb.append(adminEvent.getAuthDetails().getUserId());
    sb.append(", ipAddress=");
    sb.append(adminEvent.getAuthDetails().getIpAddress());
    sb.append(", resourcePath=");
    sb.append(adminEvent.getResourcePath());

    if (adminEvent.getError() != null) {
        sb.append(", error=");
        sb.append(adminEvent.getError());
    }

    return sb.toString();
}
````


Of course if you want to print it out a different way, you are free to change it up.
Make sure the onEvent methods call the correct toString methods.
We will keep the close method empty for now.

The listener is now done. On to the factory!

#### Event Listener Factory
The idea of the factory is the same as with the event listener: Keycloak again has an interface available that we just 
have to implement. Again, create a class and give it any name you want. Just make sure that this time it implements 
EventListenerProviderFactory.

Override the following methods (*hint: you don't have to use the default parameters of these methods*):

*	create<br> 
    return an instance of the event listener that you have made above
*	init<br>
	Over here we can retrieve extra configuration for our event listener after the factory is first created. We leave 
    this empty for now. In the next exercise we will dive deeper into it. 
*	postInit<br>
	This will be called after all the factories have been called. Leave this empty
*	close <br>
	This is called when the server shuts down. Leave this empty
*	getId<br>
	This id will be visible in Keycloak. Make sure you return "example-event-listener" if you want to use the listener 
    for the next exercise.

The factory is now also done! Now for the last part.

#### The config file
There are some important rules about this config file:

*	The file must be located in resources/META-INF/services
*	The name of the file must be the package name + class name of the factory provided by Keycloak: In our case the name of the file will be ``org.keycloak.events.EventListenerProviderFactory``. 

Note that if you have 
made multiple plugins that implements different factories, you have to make a config file for each factory.

In this file we will put all the factories that we have made that implemented the EventListenerProviderFactory. In our 
case its only one, but it can also be multiple. Make sure you write the complete path (after `src/main/java`).
For example, if the file that implements the EventListenerProviderFactory is at `src/main/java/nl/rest/of/the/path/SampleEventListenerProviderFactory.java`, then you put the following in the config file:
``nl.rest.of.the.path.SampleEventListenerProviderFactory``

That’s it, we now have a fully functional plugin for Keycloak. Create the jar file (`mvn clean package`), put it in the `providers` folder and 
start up Keycloak again to see if it is being loaded in the terminal. In the next tutorial, we will look more at how the event listener works, and how to actually deploy the plugin for it.

Of course there are loads of different types of plugins that can be created based on all the interfaces that Keycloak 
provides. These interfaces are called Service Provider Interfaces (SPI). Find the list of SPIs in the `master` realm by clicking in 
the top-right on the user name, go to `Realm info` and go to the `Provider info` tab. If you have loaded in your event 
listener, then you will also see it here under the eventListener SPI.

**[Continue to w3 of the advanced track](advanced-w3-event-listener.md)**