# Add a custom Event Listener Module

Let's extend the functionality of Keycloak. Sometimes you want an action to happen when a specified action has been done. For this you can use an Event Listener. Most things that happen in Keycloak are events, for example users logging in or out, or admins making changes.

## Example event listener

If you have created your own event listener, you can use that one. We have also created a custom event listener as an example that we will use. This listener simply outputs every event to stdout.

Git clone the sample event listener repo:
```
git clone https://bitbucket.org/first8/example-event-listener.git
```

Build the maven package:
```
mvn clean package
```


In case you haven't created the event listener in the previous exercise, check the code in `SampleEventListenerProvider.java` to get a feeling for how this is implemented. Ultimately, it is simply implementing the `EventListenerProvider` interface and then use your imagination on what you want to do with the events. In this case, we were not very creative:

```
    @Override
    public void onEvent(Event event) {
        System.out.println("Event Occurred:" + toString(event));
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
        System.out.println("Admin Event Occurred:" + toString(adminEvent));
    }

```

## Deploy

We deploy the plugin like we described in the previous tutorial: Copy `target/event-listener-25.0.1.jar` to providers `<keycloak-dir>/providers/` directory, then restart keycloak.

### Have a look at the code

If you have a look at the code, you can see that there are a few files required.

* the `SampleEventListenerProvider.java` is the actual listener that we implemented
* the `SampleEventListenerProviderFactory.java` is a factory for those listeners, it creates the listeners as Keycloak needs them
* the `resources/META-INF/services/org.keycloak.events.EventListenerProviderFactory` tells Keycloak (or rather the application server) that there is a factory that it should use of the type `EventListenerProviderFactory`. The content is the actual classname of our factory, or multiple classnames if you have multiple factories


### Configure

To configure the event listener:

* Restart keycloak.
* In the `Realm settings` of the `master` realm: Select the `Events` tab.
* In the dropdown menu, select our event listener `example-event-listener`.
* `Save`

![image](images/events-master.png)


Now you will see in the system.out the logging of events, as this is the current functionality of the event listener that you have added:

```
Event Occurred:type=LOGIN, realmId=[...]
```

### Test it

You can test this further by e.g. logging in and out, or by simply refreshing the page. Note that whenever you do so, events of type LOGIN as well as CODE_TO_TOKEN get logged.


## Add parameters
 
The example event listener can be configured to exclude certain events. Since on a busy server you may have quite a few events, it is wise to not spam your logs too much. For that, we can add parameters to our custom listener so we can configure which events to ignore. For example, to exclude REFRESH_TOKEN and CODE_TO_TOKEN events, follow these steps:

First, we need to add some code to our Factory to parse these values and pass them along to the listener:

```
public class SampleEventListenerProviderFactory ... {
    private Set<EventType> excludedEvents;
    private Set<OperationType> excludedAdminOperations;

    @Override
    public EventListenerProvider create(KeycloakSession keycloakSession) {
        return new SampleEventListenerProvider(excludedEvents, excludedAdminOperations);
    }

    @Override
    public void init(Config.Scope config) {
        String[] excludesArray = config.getArray("exclude-events");

        if (excludesArray != null) {

            excludedEvents = new HashSet<>();
            for (String e : excludesArray) {
                excludedEvents.add(EventType.valueOf(e));
            }
        }

        String[] excludesOperations = config.getArray("excludes-operations");

        if (excludesOperations != null) {
            excludedAdminOperations = new HashSet<>();
            for (String e : excludesOperations) {
                excludedAdminOperations.add(OperationType.valueOf(e));
            }
        }
    }
...

}

```

And, in our Listener, we need to use the newly created parameters:

```
public class SampleEventListenerProvider implements EventListenerProvider {
    private final Set<EventType> excludedEvents;
    private final Set<OperationType> excludedAdminOperations;

    public SampleEventListenerProvider(Set<EventType> excludedEvents, Set<OperationType> excludedAdminOperations) {
        this.excludedEvents = excludedEvents;
        this.excludedAdminOperations = excludedAdminOperations;
    }

    @Override
    public void onEvent(Event event) {
        if (excludedEvents == null || !excludedEvents.contains(event.getType())) {
            System.out.println("Event Occurred:" + toString(event));
        }
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
        if (excludedAdminOperations == null || !excludedAdminOperations.contains(adminEvent.getOperationType())) {
            System.out.println("Admin Event Occurred:" + toString(adminEvent));
        }
    }
...

```

### Test it

Create a new jar for the plugin, and replace the event listener jar in Keycloak's providers folder with the new jar. 

Define which events should be excluded anywhere in the configuration file (conf/keycloak.conf):
``` 
spi-events-listener-example-event-listener-exclude-events=REFRESH_TOKEN,CODE_TO_TOKEN
spi-events-listener-example-event-listener-excludes-operations=UPDATE
```

Now (re)start keycloak:

* Windows: ``bin\kc.bat start-dev``
* Linux: ``bin/kc.sh start-dev``

You should see that whenever you refresh a Keycloak page, you will only get an event of type LOGIN now, whereas before there would also be a CODE_TO_TOKEN event.
And when you update a field, you won't see the admin event UPDATE in the terminal anymore.

## Change the Event Listener

For the next exercise we want to be able to do some monitoring of Keycloak. For that, we need to change the event listener to count how often an event happens. The details of this exercise are left for the reader ;)

You may, or may not keep track of excluded events.
You do not need to keep track of admin events.

* create a static Map in the event listener that keeps track of the times a unique event happens (static, because listeners will be created and destroyed at will)
* for now, simply leave it in memory, no need to store it somewhere more permanent
* for debugging purposes, log the current count to stdout
* compile and deploy the new event listener and see if it works

**[Continue to w4 of the advanced track](advanced-w4-rest-crud.md)**

