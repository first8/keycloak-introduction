## Admin Rest API ##

So far you have been using our trustworthy friend the Admin Console. In a browser.
The Admin Console is an awesome and brilliant web app that fires http-requests.
To prove this theorem; open the network tab in the browsers dev-tools and in the admin console, click whatever you want.
You'll see the requests piling up.

### Introduction ####

With our network tab still open in our dev-tools, let's create a group

* Navigate to the `workshop` realm
* On the left click __groups__ and then __create group__

![image](images/create-group-console.png)

Below is the successfully completed request, copied from that network-tab, as curl: 
```
curl 'http://localhost:8080/admin/realms/workshop/groups'
-H 'Accept-Language: nl-NL,nl;q=0.9,en-US;q=0.8,en;q=0.7'
-H 'Cache-Control: no-cache'
-H 'Connection: keep-alive'
-H 'Origin: http://localhost:8080'
-H 'Pragma: no-cache'
-H 'Sec-Fetch-Dest: empty'
-H 'Sec-Fetch-Mode: cors'
-H 'Sec-Fetch-Site: same-origin'
-H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'
-H 'accept: application/json, text/plain, */*'
-H 'authorization: Bearer eyJhbGciOiJgfgd8xdy1DQ211aDJqM0hPdGJTdTZFIn0.eyJwtsAreActuallyALotLongerThanThis6NGI0Yi04NGMzL
-H 'sec-ch-ua: "Chromium";v="116", "Not)A;Brand";v="24", "Google Chrome";v="116"'
-H 'sec-ch-ua-mobile: ?0'
-H 'sec-ch-ua-platform: "Windows"'
--data-raw '{"name":"this is our groupname"}'
--compressed
```

You can analyze the contents of this yourself if you'd like.
Discussions about the choice of OS are allowed after the workshop.

* First; have a quick look at the uri in the request above, to learn its scheme.

It's quite simple, `base url` `/admin/realms/{realmname}`  
The base url is where keycloak is listening. If you followed along this workshop, you're hosting it locally, on port 8080. Which is the default.  
This makes the base-url `http://localhost:8080` followed by `/admin/realms/workshop`

If you want to, copy the *authorization* (from your own requests) and paste that into a jwt-debugger, for example http://www.jwt.io  

This will show you everything that is the used and valid token.  

![image](images/token.png)

There is a small part here that is interesting; notice the issuer (`iss`) of this token is the master realm.  
The other thing is the Authorized Party (`azp`), which is __security-admin-console__. You might have seen that name before...  


* Go back to the admin console in the browser 
* Have a look at the clients of the master realm, see anything familiar?

So, now we know that even our friend the admin console uses a client to communicate with the keycloak server.

* please note there are more default clients, including one named `admin-cli`.

---

### Delete the group created earlier ###
The examples below will use IntelliJ IDEA's httpClient and/or [Postman](http://www.postman.com) to make requests.
You can also use cUrl, or whatever you are familiar with. As long as it can send requests and it makes you happy.

#### Step 1: Get token ####
As seen in the introduction, a request needs authentication by means of a bearer token.  
When using the console, this token is obtained by logging in, and refreshed upon expiry.  
In this case the admin-console won't be used, so another way to get that token is needed.

With your preferred tool:

* Create a __post__ request to url `http://localhost:8080/realms/master/protocol/openid-connect/token`    

* Add key-value pairs using PostMan or IDEA as shown below (or another tool if you prefer.)
* In Postman (Body -> x-www-form-urlencoded):

| Key        | Value     |
|------------|-----------|
| `client_id`  | `admin-cli` | 
| `username`   | `admin`     |
| `password`   | `admin`     |
| `grant_type` | `password`  |

(If you set up keycloak with different admin credentials, use those.)

![image](images/request-token-postman.png)

* in IDEA:
    * Content-Type: `application/x-www-form-urlencoded`
    * `grant_type=password&client_id=admin-cli&username=admin&password=admin`

![image](images/request-token-idea.png)

The values above specify to which client this request is addressed (__admin-cli__) how access should be granted (__grant_type__) and the credentials to do so (__username__, __password__).  

The response should look something like this:  
(_The tokens in your request will be a lot longer than the ones in the screenshot._)

![image](images/response-token-idea.png)

* Be sure to notice the `"expires_in": 60` part.

This means that this token will expire after 60 seconds and thus be invalid. Then, you will have to get a new token...<br>

If you don't want to have to get a token so often, you can change the `Access Token Lifespan` in the `Realm settings`->`Tokens` of your master realm in the Keycloak console. Press `Save` at the bottom. 
If you changed the lifespan to 10 minutes, and send the above request again, you will see `"expires_in": 600` instead. 


* Copy the value of `access-token` to your clipboard. _do not include the quotation marks..._

#### Step 2: Delete ####
In order to delete the group created earlier, you need the UUID (id) of said group.

* Create a get request to `http://localhost:8080/admin/realms/workshop/groups`
* As authorization, use a __bearer token__ and paste in the value you just copied
![image](images/bearer-token-postman.png)

or in IDEA:

* For `Accept` and `Content-Type`, just enter `application/json`.


![image](images/request-groups-idea.png)

As a response you should receive a JSON containing all groups, including their ID...

```
{
    "id": "0ed5a4b8-aa64-4e6a-8516-a4ba2d5781cd",  
    "name": "this is our groupname",  
    "path": "/this is our groupname",  
    "subGroups": []  
}
```

* Now do a DELETE request to `http://localhost:8080/admin/realms/workshop/groups/%YOUR-UUID_GOES_HERE%`
* Go back to the admin console to verify the group is deleted.

---

### Create and update a user ###
Lastly, let's focus a bit on how the admin-rest-api deals with objects.

#### UserRepresentation ####
Below is the representation of a user. (keycloak 25.0.1)

| Name   | Schema                                   |
|--------|------------------------------------------|
| access | Map                                      |
| attributes | Map                                      | 
| clientConsents | List < UserConsentRepresentation >       |
| clientRoles | Map                                      |
| createdTimestamp | Long(int64)                              |
| credentials | List < CredentialRepresentation >        |
| disableableCredentialTypes | Set < String >                           |
| email | String                                   |
| emailVerified | Boolean                                  |
| enabled | Boolean                                  |
| federatedIdentities | List < FederatedIdentityRepresentation > |
| federationLink | String                                   |
| firstName | String                                   |
| groups | List < String >                          |
| id | String                                   |
| lastName | String                                   |
| notBefore | Integer(int32)                           |
| origin | String                                   |
| realmRoles | List < String >                          |
| requiredActions | List < String >                          |
| self | String                                   |
| serviceAccountClientId | String                                   |
| username | String                                   | 

Each of these attributes is optional. So, if we want to create a new user, not all fields above have to be included in our request.  
Some properties refer to other types of representation, for example the CredentialRepresentation.
All of these, and all other types of representations keycloak uses can be found in the [Keycloak REST-API Documentation](https://www.keycloak.org/docs-api/24.0.4/rest-api/#_overview).


* Create a POST request to `http://localhost:8080/admin/realms/workshop/users`
with the following headers:
``` 
Authorization: Bearer %YOUR_TOKEN_GOES_HERE%
Accept: application/json
Content-Type: application/json
```
, and the following body:
```
{
    "username": "CptTightPants",
    "firstName": "Malcolm",
    "lastName": "Reynolds"
}
```

* After successfully firing the request, the new user will show up in the admin console (in __Users__, obviously)

![image](images/rest-created-user.png)

As you can see, this user is not enabled, which effectively means, the user can not use this account. 
Let's fix that, and also set up an email address, and force the user to verify it, and update its password 

* Create a PUT request to update this user with this body:
```
{
    "email": "whoever@whatever.com",
    "enabled": "true",
    "requiredActions": ["UPDATE_PASSWORD", "VERIFY_EMAIL"]
}
```
* Don't forget to put the id of the new user in the url:
![image](images/rest-update-user-idea.png)
* Or in Postman:
![image](images/rest-update-user-postman.png)


Hint: You can add url parameters to GET request to filter, like this: `http://localhost:8080/admin/realms/workshop/users?lastName=Reynolds`


**[Continue to w5 of the advanced track](advanced-w5-configuring-using-ansible.md)**
