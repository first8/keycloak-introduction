# 2FA, Step-up Authentication and the Authorization flow

In this assignment we are going to build in 2-Factor-Authentication with OTP (One Time Password) in combination with step-up-authentication.
What does this mean? We still want users to be able to log in to the Hotel app using a password. However, if a user wants to delete a Hotel booking (a very critical operation), we want to make extra sure they are who they claim they are, so they must use 2FA as extra verification. 
This is the step-up: a user is already logged in *but* is required to step up their authentication to a higher level.

You will learn to:

* Modify an authentication flow
* Setup a 2-factor authentication (2FA) requirement with an One Time Password (OTP)
* Create a step-up authentication

## Prerequisite
For this assignment we're going to need an authenticator-app on our phone, like FreeOTP, Microsoft Authenticator, or Google Authenticator.
You can find these in your relevant app-store.

### Configure OTP
First, in our keycloak console (realm: `workshop`), navigate to the `Authentication` page, select the `Policies` tab, and finally select `OTP Policy`.

![OTP policies](images/otp-policy.png)

You can configure OTP here, so it complies to your use case.
In this workshop, the default settings are enough, so we just leave things be.

### Setup LoA and ACR
ACR is the abbreviation of Authentication Context Reference. These are user defined values (in the context of the client: values that your application understands).
LoA are sequential numeric values, indicating the *Level Of Authentication* that a user can have. 
A user can define ACR (Authentication Context Reference) values which can be given more descriptive names.
ACR values can then be mapped to LoA values. The application will read ACR values, not LoA.

for example:

| ACR    | LoA |
|--------|-----|
| `cub`  | 1   | 
| `lion` | 2   |


So: after logging in (with only a password), a user will have ACR `cub` with LoA `1`; after stepping up with 2FA they'll be a fully-fledged `lion` with LoA `2`.

* Go the detail page of our client `frontend`, and select the `Advanced` tab. 
* Scroll downwards to the `Advanced Settings` section.
* fill in the following values

![ARC LOA](images/arcloa-values.png)

* Press `Save`.

### Authentication flow
To determine what to do when in which order, keycloak uses _Authentication flows_.

#### Creating a flow
* Navigate to the `Authentication` page again, but this time we'll stay on the `Flows` tab.
* Click on the `browser` flow.
* Let's stop for a second and have a look at what we are seeing...

![browser-flow](images/auth-flow-browser.png)

These are the default steps a browser will follow when connecting to our Keycloak app.
On the top, there is `Cookies`, this is a step that checks for, you guessed it,
cookies, so users won't have to log in every time they click somewhere.
This part of the workshop is not about that, nor about IDP's and Kerberos. You cannot modify a default flow, so we'll first need to create a copy and then use that. 

* At the top right action menu, select `Duplicate`
* For the sake of clarity, we'll just leave the name as is, click `Duplicate`

![duplicate](images/flows-duplicate.png)

#### Setting up our custom flow

Now let's set up our new flow `Copy of browser` to use Levels of Authentication. We'll define two _conditional_ paths, one for LoA 1 and one for LoA 2. Based on the client's requested LoA (derived from the ACR), Keycloak will pick one of these paths. 

1. Delete every item other than `Cookie` and `Copy of browser forms` (but delete its subitems).
2. Click the `+` behind `Copy of browser forms` to add a sub-flow with name `cub flow`.
3. Also add a sub-flow named `lion flow` to `Copy of browser forms`:

![add-sub-flow](images/add-sub-flow.png)

4. Click the `+` behind `cub flow` to add a condition, for `Level of Authentication`. 
5. Click the gear icon, just set `Alias` to anything and `loa-condition-level` to `1` and `Save`.
6. Repeat steps 4 and 5 for `lion flow`, except enter value `2` instead of `1`.
7. Click the `+` behind `cub flow` to add a step, `Username Password Form`.
8. Add step `OTP Form` to `lion flow`.
9. Finally, *edit the values in the dropdown menus next to the items* so that the flow looks like this:

![setup flow](images/auth-flow-setup-complete.png)

#### Using our custom flow

Now we can use our new flow for Authentication instead of the default browser flow. 
From the top-right 'Action' dropdown menu, select `Bind flow` and choose binding type `Browser flow`.

![bind-flow](images/bind-flow.png)


### Testing

If you imported the Keycloak realm `workshop`, your user `aap` should have role `booking_delete` already. You can verify it in the `Users` menu by clicking on the user and then the `Role mapping` tab. 
If you created the realm manually, you will have to create the realm role `booking_delete` and add it to a user which has `booking_read` already. Having done this, you can start the steps below:

* Open the `admin` branch of the Hotel application (you can also use the `basic` branch if you completed the basic track)
* Build and run the application using the all-in-one command `.\mvnw spring-boot:run`.
* Log in like before (you probably want user `aap` with password `first8`)
* Click `Bookings` from the navigation bar. You should see this:

![bookings-no-delete](images/bookings-no-delete.png)

There is no button for deleting a booking, because besides the role `booking_delete`, the Hotel application also requires a user to be stepped up to ACR `lion`.

* Press your username at the top-right of the hotel app, then click `Step-Up`. This forwards you to a Keycloak page where you set up an app to obtain One-Time-Passwords from:

![image](images/otp-setup.png)

After setting up and entering your current one-time password, you will be redirected back to the Hotel app.

* Go back to `Bookings`: You can now use delete buttons for your bookings. Great success!

Optionally, you can verify that it works every time:

* If you log out and log in again, the delete buttons are gone again. 
* Step up again (obviously you don't need to set up the external app again for the same user).
* Welcome back the friendly delete buttons in your Booking overview.

### Before moving on to other assignments

We have to undo some of our changes here.

* Firstly, go back to the frontend client in Keycloak, then to it's `Advanced` tab, and under `Advanced settings`: Empty the fields next to `ACR to LoA Mapping` and `Default ACR Values`.
* Press `Save`.
* In the `Authentication` menu, click the `browser` flow and click `Bind flow` from the dropdown menu.
* If you don't want to have to keep using the Authenticator app for the user you already set it up for, log in as that user in the [account console](http://localhost:8080/realms/workshop/account/), go to `Account security`->`Signing in`, and press the `Delete` button next to `otp`.

**[Continue to w5 of the admin track](admin-w5-passwordless-authentication.md)**