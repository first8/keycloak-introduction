# User federation

By default Keycloak has 3 different user storage adapters. One is the internal (default) SQL database. 
The other two are _federated_ adapters:

* LDAP
* Kerberos

In this assignment we are going to setup an LDAP User Federation and see how we can extract more details from the LDAP environment and use those in Keycloak. 
First we need to connect ("bind") to the glauth server (using a connection url and a service user), and then tell Keycloak which (directories of) users to 
search and how to map them.

Let's say we want two groups to use the hotel application: `reception-view` (with roles `booking_read` and `room_read`) and `reception-admin` (with additional role `booking_update`). 
We already have two groups in our glauth server with those names, and glauth users in those groups.
So after connecting to the glauth server, we want to assign the correct keycloak roles to our new users. We'll do that by mapping the glauth groups to Keycloak groups, then assigning roles to the Keycloak groups.
As an additional exercise, we also show how to directly map the glauth groups to Keycloak roles, without needing to use any Keycloak groups.


## LDAP

As said before, one of the federated storage adapters is LDAP. With LDAP the users will be synchronized to the Keycloak local storage, except for passwords. Password validation is delegated to the LDAP server.

Synchronization can be done manually or scheduled in a background task.

Advantages:

* You can store extra attributes/information per user in Red Hat SSO, that are not in the LDAP system.
* It reduces load on the LDAP server, as the 2nd time users are accessed they are loaded from the Keycloak Database.
* The only load on your LDAP server is password validation (and synchronization which you can plan).

Disadvantages:

* The import needs to be synchronized with LDAP changes.
* When first queried the chosen approach requires Keycloak to create records in its database which makes it a bit slower.

## Setup an LDAP server

We will use GLAuth as a light-weight LDAP server (is that a light-weight light-weight directory access protocol server then?). The charm of this server is that you can easily edit its directory configuration. 

* Download the `glauth` binary from https://github.com/glauth/glauth/releases (we have tested up to version 2.3.2)
* Find the `glauth-conf.cfg` in the assignments that you cloned, and make sure it is in the same place as the glauth binary.
* Run it as follows (the name of your executable may be different, rename it to `glauth` first if you want to follow these literally):
    * On unix-like systems: `chmod +x glauth; ./glauth -c glauth-conf.cfg`
    * On Macs: `chmod +x glauth; glauth -c glauth-conf.cfg`
    * On Windows: `.\glauth -c glauth-conf.cfg`
    * If you run into trouble, see some additional tips below
* Note that glauth will log the port it is listening on; you may need this later:
```
Tue, 26 Sep 2023 11:52:04 +0200 INF LDAP server listening address=localhost:3893```
```

### Connect LDAP to Keycloak

1. Navigate to Keycloak Console: [http://localhost:8080/admin/](http://localhost:8080/admin/)
2. Select the `workshop` realm
3. In `User Federation`, select`Add Ldap providers`.

![image](images/keycloak-ldap-new.png)

4. Provide the required LDAP configuration details below (***warning: you may have to remove trailing whitespaces if you copy-paste***):


| Field                                        |Value|Notes|
|----------------------------------------------|---|---|
| ***General options***                        | | |
| Vendor                                       | `Active Directory` | The LDAP provider you are using |
| ***Connection and authentication settings*** | | |
| Connection URL                               |`ldap://localhost:3893` | If this does not work, check the _glauth_ console for the correct value, it logs something like `LDAP server listening on localhost:3893` and you need to prepend the ldap protocol|
| Bind Type                                    | `simple` |  |
| Bind DN                                      | `cn=serviceuser,ou=svcaccts,dc=first8,dc=com`| DN of the administrative or service user that accesses the information to use. Note that this user is in a different node.  |
| Bind Credential                              | `mysecret` |Credential/Password used for the connection |
| ***LDAP searcing and updating***             | | |
| Edit Mode                                    | `READ_ONLY` |   |
| Users DN                                     | `ou=users,dc=first8,dc=com`  | The full DN of the LDAP tree where your users are located. The DN is the LDAP user parent. This is *not* a group DN. You must specify a node that contains users. |
| Username LDAP attribute                      | `cn` | Attribute that contains the user name, in our case leave as `cn` |
| RDN LDAP attribute                           | `cn` |   |
| UUID LDAP attribute                          | `uidNumber` | Attribute in LDAP that holds a unique value. In our case set to `uidnumber` |
| User Object Classes                          |`posixAccount`| glauth has 'posixAccount' objects, other common names are `person, organizationalPerson, user`|
| User LDAP Filter                             | _leave empty_ | Used to filter the full list of users in the Users DN node to just the users you want to import into Keycloak. Can use a filter like (mail=*) to only include users with an email address. |
| Search Scope                                 | `Subtree` |Subtree or One Level |   


5. Click `Save`
6. Click on the newly created `ldap`:

![image](images/ldap-created.png)

7. At the top-right action menu, choose 'Sync all users':

![image](images/ldap-sync-users.png)

8. Check Keycloak's *Users* menu to verify the glauth users are now included: Enter `*` in the search bar and press `refresh` or `->` to see all users. Besides your alter ego *aap*, you should see Jane and John Doe, and a *serviceuser* and *administrator*.
9. Check the logs as well; Keycloak should log how many users it imported. ("imported" is a misnomer, as these users are not persisted in Keycloak unless the glauth server is running.)

### Map LDAP Groups to Keycloak Roles

In this section we are going to import groups as defined in LDAP into Keycloak. 

In your `workshop` realm, go to `Realm roles` to verify that you have the roles `booking_read`, `booking_update` and `room_read`:

![image](images/keycloak-roles.png)

If for some reason you don't have these, add them using the `Create role` button.

### Import LDAP Groups

1. Go to `User Federation` and select the ldap federation you just created.
2. Select the tab `Mappers`: 

![image](images/ldap-mappers.png)


3. Press the button `Add mapper`
4. Give the mapper a name i.e. `group ldap mapper`
5. Select Mapper Type: `group-ldap-mapper`
6. Fill in the fields:


|Field|Value|Notes|
|---|---|---|
| LDAP Groups DN | `ou=groups,dc=first8,dc=com` | The query to use for finding groups |
| Group Name LDAP Attribute | `uid` | The attribute on the group to use as a name |
| Group Object Classes | `groupOfUniqueNames` | The object class as used for groups |
| Membership LDAP Attribute | `uniqueMember` | The attribute on the group that refers to a member |
| Membership Attribute Type | `DN` | The type of reference from a group to a member |
| User Groups Retrieve Strategy | `LOAD_GROUPS_BY_MEMBER_ATTRIBUTE` | The strategy to load the groups and users (start from a group in this case)|
| Member-Of LDAP Attribute | `memberOf` | The attribute on the user that references the group (used in `LOAD_GROUPS_BY_MEMBER_ATTRIBUTE` strategy)  |


7. Click `Save`

   *Note: if you get weird errors (by importing or by viewing groups), you probably picked a wrong Group Name LDAP Attribute. Keycloak doesn't handle null values there well, so you will need to delete the corrupted groups. Easiest way is to turn ON
   `Drop non-existing groups during sync`, making sure that Keycloak starts off with a clean sheet everytime.*

8. Select the mapper we just created from the list.
9. From the drop-down list on the top right, select `Sync LDAP Groups to Keycloak`
10. Using the 'Users' and 'Groups' menu items, check that the user `administrator` is in the group `reception-admin` and `reception-view`.

### Map Group to Role
1. Go to `Groups`
2. Select the group `reception-view`
3. Select the tab `Role mapping`
4. Press `Assign role`
5. Select `booking_read` and `room_read` and click on `Assign`
6. Verify that the users Jane and John Doe now have the roles `booking_read` and `room_read`. (Hint: you may need to view inherited roles).
7. Go back to the `Groups` menu
8. Select the group `reception-admin`
9. Select the tab `Role mapping`
10. Press `Assign role`
11. Select `Filter by realm roles`
12. Select the `booking_update` and click on `Assign`
13. Verify that the user `administrator` now all three roles `booking_update`, `booking_read` and `room_read` (the administator is in both groups.)

### Testing with the app
You should now be able to test that everything works in the Hotel app at [http://localhost:8000](http://localhost:8000)

* Recall that creating a booking is for any user, even anonymous guests.
* The users Jane and John Doe (`janedoe` and `johndoe`, passwords are `dogood`) should now be able to log in. They can view bookings/rooms from the navigation bar:

![image](images/hotel-navbar.png)

* The user `administrator` (password is also `dogood`) should be able to edit a booking by clicking its green Edit button in the Booking overview. This is because (s)he is in the imported `reception-admin` group with role `booking_update`.


# LDAP roles without the groups

In this exercise we imported first some groups, and then assigned the role we want to the group. We could have done this directly, by mapping an LDAP group to a Keycloak Role.
Before doing so, remove the `group-ldap-mapper` (from `User Federation`->`ldap`->`Mappers`) and the two imported groups (from the `Groups` menu.)

Now add a `role-ldap-mapper` instead, following similar steps as for the group mapper. The imported roles are added to the users directly, and the role names are the same as the ldap group names, e.g. `reception-admin`. These roles are not linked to any privileges in the Hotel app.

# Additional tips

## Running glauth on a Mac
For Intel Macs, you need the 'glauth-darwin-amd64' binary. If you try to run it from the command line, and you get a message saying: *"glauth-darwin-amd64” cannot be opened because the developer cannot be verified.*, you need to do the following to trust that binary:

* Open System Settings
* Go to Security & Privacy
* Scroll down a bit until you see the same message with an `Allow Anyway` button
* Press that button, and possibly authenticate yourself :)

## Keycloak logging
To see a bit more logging, you can enable TRACE logging for the LDAP storage provider. To do so, run keycloak with:

`bin/kc.sh start-dev --http-port 8888 --log-level=org.keycloak.storage.ldap:TRACE`


## LDAP query tools

You can download an LDAP query tool, e.g. the Mac has ldapsearch on the command line by default. Using that, you can simulate the queries Keycloak does:

E.g., Keycloak should query the users with something like this:
`ldapsearch -LLL -H ldap://localhost:3893 -D cn=serviceuser,ou=svcaccts,dc=first8,dc=com -w mysecret -x -b "ou=users,dc=first8,dc=com" "(&(objectclass=posixAccount))"`

And for the groups:
`ldapsearch -LLL -H ldap://localhost:3893 -D cn=serviceuser,ou=svcaccts,dc=first8,dc=com -w mysecret -x -b "ou=groups,dc=first8,dc=com" "(&(objectclass=groupOfUniqueNames))"`

## Accessing the keycloak database
By default, keycloak runs on a h2 database, located in `data/h2/keycloakdb.mv.db`. 
If you need to see what is in the database, use e.g. DBeaver or your IDE to connect to an embedded h2 database. A JDBC url like this `jdbc:h2:/<KEYCLOAK_LOCATION>/data/h2/keycloakdb.mv.db`  with username 'sa' and password 'password' should do the trick. Note, you probably cannot access the database while keycloak is running due to locking issues.

**[Continue to w4 of the admin track](admin-w4-step-up-authentication.md)**