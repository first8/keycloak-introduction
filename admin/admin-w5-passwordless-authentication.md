# Passwordless Authorization

In this assignment, we are going to add passwordless authentication as an alternative to our usual Username+Password login form.

## Authentication Flow

To determine what to do when, and in which order during authentication, keycloak uses _Authentication Flows_. We will be making a custom flow in our `workshop` realm.

### Make your flow

To make things easier, we will duplicate an existing flow before editing it. 

* Navigate to the `Authentication` page.
* Click the `browser` flow.
* In the top-right action menu, select `Duplicate` and give your custom flow a name.
* In the new flow, delete the row `%your-flow-name% forms` (all of its subitems will disappear as well.)
* Click the `Add sub-flow` button, give it a name (we'll go with name "W5 Forms"), and press `Add`.


* Click the `+` next to "W5 Forms" to add a (nested) sub-flow to it, called e.g. "Password-subsub".
* Similarly, add "Passwordless-subsub".
* Click the `+` next to  "Password-subsub" and add the step `Username Password Form`.
* Click the `+` next to  "Passwordless-subsub" and add the step `Username Form`.
* Click the `+` next to  "Passwordless-subsub" and add the step `WebAuthn Passwordless Authenticator`.
* Change the `Requirement` dropdowns next to the steps so that the flow look as follows:

![image](images/flow.png)

### Enabling WebAuthn and your new flow

* From the `Action` dropdown menu at the top-right of your flow's page, select `Bind flow`:

![bind-flow](images/bind-flow-passwordless.png)

* Choose binding type `Browser flow`.
* Go back to the main `Authentication` menu, but select the `Required actions` tab this time.
* Make sure `Webauthn Register Passwordless` is enabled.

### Test

* Open the `admin` branch of the Hotel application (you can also use the `basic` branch if you followed the basic track)
* Build and run the application using the all-in-one command `.\mvnw spring-boot:run`.
* Go to [http://localhost:8000](http://localhost:8000) and login the usual way with any user.
* Press your username at the top-right of the hotel app, then click `Security`. This forwards you to the `Signing in` Keycloak page for your account:

![image](images/security-dropdown.png)

* Under `Passwordless`, click `Set up Passkey`.
* Click `Register`.
* Select one of the options as a passkey (the options depend on your OS configuration.) 
* When you're done, you can verify that a Security Key (passkey) has been added to your Keycloak account:

![image](images/passkey.png)

* Close your profile and sign out of the Hotel app.
* Go to the login form again, but this time click `Try Another Way`:

![image](images/try_another.jpg)

* click `Username` to log in without password, using your Security Key instead.

### Explaining our Authentication flow

Let's review the flow we just made:

![image](images/flow.png)

First, note we left the three default steps at the top. We can ignore Kerberos, since it's Disabled by default. `Cookie` ensures that once already logged in, you don't need to log in again on every page/refresh. This is the first Alternative in our flow: It only gets skipped when it doesn't apply, i.e. when you haven't logged in yet, or if your login session (access token) has expired. 

The next Alternative step, `Identity Provider Redirector` gets skipped in our case, since it requires additional configuration. (Unrelated to this assignment: If you have added a third party IDP as in [admin-w2-third-parties](admin-w2-third-parties.md), you can set a default IDP, which users will be forced to authenticate with, overriding the *Alternative* steps below it in the flow.) 

Next comes the sub-flow we added, "W5 Forms". As its requirement is _Alternative_, it is only used if all _Alternatives_ above it do not apply. 
We also have two _Alternative_ sub-flows under "W5 Forms" itself, but at this level of nesting it's not necessarily the case that only one _Alternative_ applies. 

Because the first nested sub-flow is "Password-subsub" with step `Username Password Form`, the default login form we see requires a username and password. 
Our second nested sub-flow "Passwordless-subsub" includes the [WebAuthn](https://en.wikipedia.org/wiki/WebAuthn) step, which provides the passkey and was accessed through "Try Another Way" and "Username". To make this the default instead, we can simply change the order of the two nested sub-flows by dragging and dropping.

## Extra Challenge
If you have previously done the [admin-w4-step-up-authentication](admin-w4-step-up-authentication.md) tutorial, you have already made a custom authentication flow. It used 2FA with an external app to "step up" from a first level of authentication to a second level. But now we use the flow from this assignment as browser flow instead of that flow... Is there a way to combine the two?

If you're up to the challenge to see whether you have fully grasped the two assignments: *Try to combine the two flows.*
Then bind the flow from the top-right action menu, and run the Hotel app. If it's not still running from the `admin` branch, start it up using `./mvnw spring-boot:run`.

*You will have to repeat section [Setup LoA and ACR](admin-w4-step-up-authentication.md#setup-loa-and-acr) from the admin-w4 assignment to make the flow work.*

To test: Log in using the passkey (passwordless), then step-up your authentication using OTP, as in admin-w4. (You will probably have to delete your old "aap" account from the authenticator app, since it was unlinked from "aap" in Keycloak at the very end of admin-w4.)

### Solution

(If you have imported the `workshop` realm, you can find the solution in the authentication flow `admin-w5-extra-solution`.)

Duplicate/edit the step-up flow from the previous assignment, but add the two nested sub-flows from this assignment to the `cub` level of authentication:

![image](images/extra_solution.png)

**You have finished the admin track of the workshop!**

If you are interested in the other tracks, check out the [README](../README.md), or jump straight to the start of another track (w0 and w1 can be skipped):

* [basic-w2: Oauth2 Glossary](../basic/basic-w2-oath2-glossary.md)
* [advanced-w2: Write and install plugins](../advanced/advanced-w2-write-and-install-plugins.md)