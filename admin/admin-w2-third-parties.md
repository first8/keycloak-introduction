# Third Parties: IDP Connect

You now have basic knowledge of how Keycloak works and how to integrate it. But one of the main benefits of Keycloak is of course the Single Sign On functionality.
In this task we are going to connect a third party _identity provider_ (known as an IDP).

_Please note that providers occasionally change their appearances so the screenshots and flow below might be outdated. If so, please notify us, so we can update it._

## GitHub
For this part, you'll need a GitHub Account. If you do not have any , you can create one if you like. Else, just read through the steps to see what is happening, or if you're brave, you can try with another provider where you do have an account.

* Open the Keycloak admin console [http://localhost:8080/admin/](http://localhost:8080/admin/)
* Go to our `workshop` realm and click **Identity Providers** in the left menu.

![image](images/identity-providers.png)

* Click on the GitHub button.
* On the following screen, copy the **Redirect URI**

* Open a new browser tab and go to [http://www.github.com](http://www.github.com) and log in.
* Click your profile icon and select **settings**

![image](images/idp-github-settings.png)

* On the settings page, select **Developer settings** at the bottom of the menu on the left

* Now, select **OAuth Apps** in the menu on the left, and click **New OAuth App**.

![image](images/idp-github-addapp.png)

* Fill in the fields
  * **Application name**: `first8 keycloak introduction`
  * **Homepage URL**: `http://localhost:8080`
  * **Authorization callback URL**: `paste your copied redirect URI here`

* Click **Register Application**

![image](images/idp-github-ids.png)

* Generate a new client secret. **You will only see this secret once**
* Copy the secret
* Paste it in the corresponding field in our keycloak console
* Similarly, copy and paste the Client ID.

![image](images/idp-github-addsecret.png)

* Click **Add**

And that's all.

## Test

* Make sure the branch `admin` of the Hotel application has been built and is running (unless you solved the basic track, then the `basic` branch is interchangeable with the `admin` branch). 
* Back to our app at [http://localhost:8000](http://localhost:8000)
* If you are still logged in as our manually created user, log out.
* Click the **Sign in** button

![image](images/idp-github-login.png)

* Click to sign in with GitHub
* Authorize our app and log in with your GitHub credentials

* In the admin console, go to **Users** and you'll notice a new user has been added, with your GitHub username.

**[Continue to w3 of the admin track](admin-w3-ldap.md)**