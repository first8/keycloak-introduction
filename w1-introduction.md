# Creating your first Realm
 
A Keycloak realm is a self-contained domain or partition within Keycloak that represents a security and administrative boundary. It's a logical grouping of users, applications, and identity providers, and it defines a set of authentication and authorization policies.
This is exactly what we need, so let's create a new realm for our hotel app! 

Followers of the **basic** track can skip ahead to section ["For the Basic track"](#for-the-basic-track), whereas **advanced** and **admin** followers can read on. (If you're not sure which track you want to follow, refer back to the [README](README.md).)

## For Advanced and Admin tracks:

* In the admin dashboard click on the realm name in the top left
* Click on the `Create Realm` button
* Now click the `Browse...` button next to `Resource file`

![image](images/create-realm-21.png)

* Select the `workshop-realmexport` file inside this repository
* Click on the create button
* When you now navigate to clients in this realm, there should be a client called frontend.

The `workshop` realm we just imported includes a user with all the necessary realm roles for the **admin** track (you don't need to log in to the `workshop` realm in the **advanced** track):

* Username: `aap`
* Password: `first8`

Followers of the **admin** track can now use the hotel application without getting a warning about Keycloak. 

### Start your track!

Now that you have finished w0 and w1 of the workshop, the setup part is done. You are ready to start your track of choice!

* [**Advanced track:** Continue with advanced-w2: Write and install plugins](advanced/advanced-w2-write-and-install-plugins.md)
* [**Admin track:** Continue with admin-w2: Third parties](admin/admin-w2-third-parties.md)

## For the Basic track:

* Open the Keycloak admin console
* Go to the realm selector dropdown menu in the top left corner.
* Click it and select **Create Realm**.

![image](images/add-realm-menu-24.png)

* All you need to do here is give the realm a name: `workshop` and click **Create**.

### Users

Before we can authorize any user, we need users. It will probably take a long time to wait for actual users to find their way to our local application. We'll create one ourselves instead, to speed things up...

* Click on **Users** in the side menu
* Click **Create new user**

* All you'll need to fill in is a _username_: `aap`
* Click on **Create**.  

A new user is now created. A user detail page will be displayed.

* Open the **Credentials** tab in the top menu
* Click the **Set Password** button

![image](images/set-password.png)

* Fill in the following details
* _Password_: `first8`
* _Password confirmation_: `first8`
* _Temporary_: `off`

* Click on **Save**, and **Save Password**.

### Start your track!
Now that you have finished w0 and w1 of the workshop, the setup part is done. You are ready to start the basic track!

* [**Basic track:** Continue with basic-w2: Oauth2 Glossary](basic/basic-w2-oath2-glossary.md)