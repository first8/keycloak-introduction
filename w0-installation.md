# Setting up your environment

*If you have completely read the document you received per mail, you may skip this and instead __[continue with w1-introduction](w1-introduction.md)__*.

In this workshop we'll set up a Keycloak environment for a small hotel application, where users can create/edit/delete bookings and assign rooms to them.
The goal of the **basic** track is to restrict all functionality to authorized hotel staff, *except* the creation of a new booking: any anonymous guest should be able to do that part. The **admin** track already starts with the secured version of the app, while the **advanced** track does not use the hotel app at all.

The next sections will tell you about everything we need to complete all the assignments:

* Java 21
* Keycloak 25
* Git
* Demo app: First8 Hotel (only for the basic and admin tracks)

## Java

Both our demo application and Keycloak use Java, so let's make sure the right version is being used.

### Windows

We can download the JDK installer for Java 21 [here](https://www.oracle.com/java/technologies/downloads/#jdk21-windows). It's recommended to install it to a pathname without spaces. Once installed,
`java -version`
should display the right version number.

We should add a _JAVA_HOME_ variables to the Windows environment using system variables. Its value should be the path to your JDK folder.

Then we update the PATH variable by appending the JDK's bin folder: `%JAVA_HOME%\bin`.

Make sure you restart all terminals and programs that (will) use Java.

### Linux
We can download the JDK installer for Java 21 [here](https://www.oracle.com/java/technologies/downloads/#jdk21-linux).

Make sure the _JAVA_HOME_ environment variable points to the new folder, and add its `bin` to the path. One way to do so is by adding the following to your `.bashrc` file:

``` 
export JAVA_HOME="/usr/lib/jvm/jdk-21.x.y"
export PATH="$JAVA_HOME/bin:$PATH"
```

Make sure you restart all terminals and programs that (will) use Java, or source .bashrc in them.

## Keycloak and/or Red Hat SSO

* Download Keycloak (version: between 21 and 25, **not 26**): [https://www.keycloak.org/archive/downloads-25.0.6](https://www.keycloak.org/archive/downloads-25.0.6), or a corresponding Red Hat build of Keycloak.
* Unzip it
* Run it on Linux/Mac with `bin/kc.sh start-dev`, or on Windows with`bin\kc.bat start-dev` (you will need Java for this)
* Navigate to [http://localhost:8080/](http://localhost:8080/)
* Create your admin account using the form
* Log in with the account you just created

## Git

### Windows

To install Git on Windows we need to go to the website [https://git-scm.com/](https://git-scm.com/) and install Git from the download link. To check if Git is working on your machine use the command: `git --version`

### Linux

We can run the following command to install the latest Git version.

`$ sudo apt-get install git`

This will take a few minutes to download. Once downloaded, we can run the `git --version` to verify our installation.


## Only for Admin and Basic track: Demo Application

For this workshop, we have a demo app that you can secure using your Keycloak knowledge: The First8 Hotel App!
You can download it from the repo: 
`git clone https://bitbucket.org/first8/keycloak-workshop-first8hotel.git`

For the **basic** track check out the `basic` branch, for the **admin** track you need the `admin` branch.

This is a Java Spring Boot app with a bundled React frontend. You can start the bundled application by executing 
`./mvnw spring-boot:run` from the project root. This command first locally installs NodeJS, NPM, and the needed dependencies. That may take a minute the first time around. 
The command then packages the frontend files and places them into the proper backend folder, then compiles and runs the backend. It's an _all-in-one_ command.

*If you want to use your own Maven instead of the Maven wrapper `mvnw`, you will need to make sure Maven uses Java 21.*

The frontend will be available via [http://localhost:8000](http://localhost:8000). Followers of the **basic** track should be able to do anything they want, since they start with the unsecured `basic` branch of the application. 
For now, **admin** track followers are confronted with a "Keycloak error" message, but can use the secured app after the next part of the workshop: Setting up Keycloak.

**[Next: finish the setup before choosing your track](w1-introduction.md)**