# Role management

We are now able to authenticate users in our application. However, functionality is still not secured. We need to add some authorization for that.

In Keycloak we can restrict access to functionality by using roles, which can be assigned to users. In this task we will create roles to protect the read and update permissions for our hotel application.

### Keycloak config

* In Keycloak go to the realm _workshop_ and select **Realm roles** in the side menu
* Click on the **Create Role** button
* Create a role named `room_read`
* Create a role named `booking_read`
* Create a role named `booking_update`

![image](images/create-realm-role.png)

* Click **Users** in the side menu
* Select the user we created earlier
* Go to **Role Mapping** in the top menu
* Select `Filter by realm roles`
* Assign the role `booking_read` to the user.

![image](images/assign-role.png)

### Backend config

* In our codebase, go to the file: `BookingController.java` (src\main\java\nl\first8\workshop\keycloak\controllers\BookingController.java)
* Uncomment the lines under the `// ----- keycloak w4: activate (uncomment) -----` lines
* Do the same in `RoomController.java` (src\main\java\nl\first8\workshop\keycloak\controllers\RoomController.java)

This newly uncommented block in shows a very simple way to add security logging to our hotel-app's terminal:

``` 
// ----- keycloak w4: activate (uncomment) -----
final SecurityContext sc = SecurityContextHolder.getContext();
logger.info( "Current user roles:" + sc.getAuthentication().getAuthorities());
```
We randomly put the above logging of current user roles in the endpoint from which bookings are retrieved, but you can put it anywhere in your backend.

The `@PreAuthorize` annotations we uncommented secure each endpoint with a check on roles the user has. 
There is one exception though. As mentioned before, any guest on the site without an account should be able to make a new booking. 
So there should be no need for a role to `POST` to `/bookings`.

*But* we can't leave it at that: After all, while any guest user should be able to make a booking, it will become a mess if anyone can assign their own room. 
Assigning a room to a booking is the job of a reception employee with the role `booking_update`.
So instead of securing the whole endpoint, we want to secure part of the functionality.

In `AssignmentService.java` (src\main\java\nl\first8\workshop\keycloak\services\AssignmentService.java), find the method `assignRoomIfAuthorized`.
Comment and uncomment as usual:

``` 
// ----- keycloak w4: comment out/remove -----
//boolean hasRoleBookingUpdate = true;

// ----- keycloak w4: activate (uncomment) -----
boolean hasRoleBookingUpdate = SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority("ROLE_BOOKING_UPDATE"));
```


### Frontend config

* Go to file `main.jsx` (src\main\frontend\src\main.jsx)
* Comment out the line under `// ----- keycloak w4: comment out/remove -----`
* Uncomment the lines under `// ----- keycloak w4: activate -----`
* This changes our router to show a page that says "unauthorized" if we browse to `localhost:8000/bookings` when a user is not logged in or does not have the role `booking_read`.

We just saw that we can secure an entire endpoint in the definition of our router. 
You can use the same construction we just uncommented in the router to conditionally render either of two alternative components, like

> _(Condition) ? ComponentA : ComponentB_

If you don't need to show any alternative if the _Condition_ isn't met:

> _(Condition) && Component_

In many places in our code, components are wrapped in `RenderIfAuthorized`, which should do the same as the above, where the _Condition_ is that you need a certain role to see the component.
But if we look at `RenderIfAuthorized` in `Utils.jsx` (src\main\frontend\src\Utils.jsx), we see that it just checks whether the user is authenticated.
To also make it look at roles, uncomment the part under the `// ----- keycloak w4: activate (uncomment) -----` line:

``` 
export function RenderIfAuthorized({ children, roles, needsStepUp = false }) {
  if (
    keycloak.authenticated
    // ----- keycloak w4: activate (uncomment) -----
       && roles.some((role) => keycloak.hasRealmRole(role))
       && (needsStepUp ? keycloak.idTokenParsed?.acr === "lion" : true)
  )
    return children;
}
```
We don't need to understand the last line we just uncommented; just know that we cannot delete a booking until we've done a later assignment.

Now you have successfully secured your application with roles.

## Testing

Compile and run the application again with our all-in-one command `./mvnw spring-boot:run`.

Yet again... open our app on [http://localhost:8000](http://localhost:8000)
![image](images/secured-home.png)

If you're still logged in from before, logout. You'll see that the menu option for "Bookings" is gone. 
If you manually try to go to [http://localhost:8000/bookings](http://localhost:8000/bookings), you'll see the page that says "unauthorized" which we put in the router.

If you log in, you can see the list of bookings again:

![image](images/just-bookings.png)

To see the edit buttons in the list again, we go back to keycloak console to add the role `booking_update` to user aap...

![image](images/back-to-user.png)

... and refresh the hotel page to see the green edit buttons in the left column:

![image](images/bookings-edit-buttons.png)

Also note that in the terminal of the application, we can verify that aap now has the role `booking_update` (and didn't in earlier logging.)

![image](images/role-logging.png)

(Optionally, if you want very extensive logging, you can also uncomment the second line of `application.properties` (src\main\resources\application.properties):
```
logging.level.org.springframework.security=trace
```

Since we added role `booking_update`, we can also select a room when we make a new booking, whereas before we couldn't (just like any unauthenticated user when they create a booking):

![image](images/form-room-buttons.png)

Finally, if you want to see the Rooms in the menu again, you can add the role `room_read` to your user, and refresh the page. Hover over "Rooms" in the navigation bar to see which rooms are assigned:

As mentioned before, we will reintroduce the possibility to delete bookings in a later assignment. Other than that, we have access to all original functionality, but now secured with all kinds of checks for authentication and authorization. Great job!


**[Continue to w5 of the basic track](basic-w5-templates.md)**