## OAuth2 basics

Before we move on with the basic track of the workshop, it is a good idea to first look at the basics of logging in through OAuth2.0. We will discuss the flow that you will experience in a lot of cases: the authorization grant flow.
Below you can find it in detail:

<img src="images/Oauth-flow.png" width=50%>

Lets first look at components on the top:

* The resource owner has access to an API and can delegate access to that API. The resource owner is usually a person and is generally assumed to have access to a web browser.
* The protected resource is the component the resource owner has access to.
* The client is the piece of software that accesses the protected resource on behalf of the resource owner.
* The authorization server is trusted by the protected resource to issue special-purpose security details – called OAuth access tokens – to clients. This will be Keycloak.

Now we Continue to the flow.
Imagine you as resource owner want to access your pictures somewhere online. The location where your pictures are stored is considered the protected resource in the flow above. The application that loads the pictures is the client.
However, you need to prove that you are indeed the right person that is allowed access to these pictures. The flow will then go as follows:

* You (the resource owner) ask the application (the client) that you would like to see your pictures.
* The application redirects you to the authorization server (Keycloak) so you can get authorized.
* You exchange your credentials with the authorization server (the client is not involved in this!). During this process you also let the authorization server know you delegate some of your authority to your application.
* If the credentials match then the authorization server sends an authorization code back and redirects you back to the application

Note that during authorization that the application is not involved in this. Imagine you have to give your password to the application. Who knows what that application can do with your credentials!

The authorization server agrees that your application can have access to your pictures on your behalf. Now we want our application to actually ask for access with the authorization code that we just received. This is called the client credentials flow:

* The application sends the authorization code and its own credentials to the authorization server
* The authorization server checks the credentials and the code. If this matches, it will send back an access token back to the application.
* The application sends the access token to the protected resource (the place where the pictures are stored)
* The protected resource sends the pictures back to the application.

We finally have access to our pictures! Keep in mind that despite this looks like a lot, this entire flow will be done in mere seconds.

**[Continue to w3 of the basic track](basic-w3-securing-1st-app.md)**