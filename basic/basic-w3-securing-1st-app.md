# Securing your first application

## Configure keycloak
Applications use a '_Client_' to communicate with the Keycloak realm. These clients obviously need some configuration.

* In the side menu click on **Clients**.
* Click on the button **Create client**.
* Fill in the _Client ID_: `frontend`
* Click **next**
* Click **next** again (we don't  need to change Client authentication, Authorization, or Authentication flow.)
* Now fill in the fields
    * _valid redirect Uris_: `http://localhost:8000/*`,
    * _web Origins_: `*`,
* Click **Save**

![image](images/frontend-client.png)


Just like with users, after creation a details page will be shown. For now, we don't need it.

## Integrate applications with Keycloak

Now Keycloak knows where to listen. It's time to tell our apps where to talk to.

### Integrate frontend

Make sure you are using the `basic` branch of the hotel app.

Find the longest line (87) in `pom.xml`, which starts with `<arguments>install`. This is the list of libraries that are arguments for `npm install`. 
Add `keycloak-js@^25` to the front of the list:

```<arguments>install keycloak-js@^25 react-icons@5.2.1 ...etc```

This `keycloak-js` (a.k.a. the Keycloak adapter for JavasScript) library is compatible with any language/framework that uses JavaScript, such as React, Vue, Angular, etc.

Next, open `main.jsx` (src\main\frontend\src\main.jsx) in your favorite editor, and uncomment the last import by removing the `//` in front of it, to start using the Keycloak object from that same library `keycloak-js`:
```
// ----- keycloak w3: activate (uncomment) -----
import Keycloak from "keycloak-js";
```

We have to configure and declare the Keycloak object. First, comment out (or remove) the "keycloak" placeholder:

``` 
// ----- keycloak w3: comment out/remove -----
// export const keycloak = { authenticated: true };
```

And uncomment the next part to declare "keycloak" as an actual Keycloak object with some configuration:

```
// ----- keycloak w3: activate (uncomment by deleting /* and */) -----
const keycloakConfig = {
  url: "http://localhost:8080/",
  realm: "workshop",
  clientId: "frontend",
};
export const keycloak = new Keycloak(keycloakConfig);
```

This configuration tells us where Keycloak can be found, which Realm to use and the ID of our Client.

Now we need to initialize the Keycloak object.  
For this you need to comment out the following code, which renders the root component of the frontend:

``` 
// ----- keycloak w3: comment out/remove -----
const router = createRouter();
renderRoot(router);
```

... and replace it with the keycloak initialization:

```
// ----- keycloak w3: activate (uncomment) -----
export let isKeycloakInitialized;

keycloak
  .init({
    onLoad: "check-sso",
    pkceMethod: "S256",
    messageReceiveTimeout: 1000,
  })
  .then(() => (isKeycloakInitialized = true))
  .catch((error) => {
    console.error(
      "keycloak.init error, make sure the Keycloak server is running and you have completed the setup of the Keycloak realm.",
      error,
    );
    isKeycloakInitialized = false;
  })
  .finally(() => {
    const router = createRouter(isKeycloakInitialized);
    renderRoot(router);
  });
```

Note that the `.finally` block calls the functions `createRouter` and `renderRoot` like before, but this time we pass boolean `isKeycloakInitialized` to `createRouter`.
The idea is to create a different router based on whether keycloak was successfully initialized or not, because we want to handle both cases differently.

Comment out the current header of createRouter:
``` 
// ----- keycloak w3: comment out/remove -----
// function createRouter() {
```

to replace it with the new header and the if-statement for the case where keycloak is not initialized:
```
// ----- keycloak w3: activate (uncomment) -----
function createRouter(isKeycloakInitialized) {
  // Every page is the same if keycloak is not initialized: A limited homepage with a warning about keycloak not having been set up.
  if (!isKeycloakInitialized) {
    return createBrowserRouter([
      {
        path: "*",
        element: <HomeWarningNoKeycloak />,
      },
    ]);
  }
```

#### The navigation bar ####

In the unprotected app, the navigation bar at the top of the page was just a menu that looked like this:
![image](images/navbar-unprotected.png)

Now that we have initialized keycloak, we want to add a button which allows the user to...

* If not authenticated yet: Log in using keycloak
* If already authenticated: Show a dropdown menu with keycloak options

So, let's make that happen:
Open `Navbar.jsx` (src\main\frontend\src\components\Navbar.jsx).

1. Uncomment the last import statement:
``` 
// ----- keycloak w3: activate (uncomment) -----
import { isKeycloakInitialized, kc } from "../main.jsx";
```

2. Uncomment the long block under the next `// ----- keycloak w3: activate (uncomment) -----`. This includes the components for the login button and dropdown menu (and step-up functionality, but that is for a later assignment.)

3. Uncomment this block near the bottom of the file to actually render either the button or the dropdown menu as part of the navigation bar (or neither, if keycloak wasn't initialized): 

``` 
// ----- keycloak w3: activate (uncomment) -----
keycloak.authenticated ? (
  <DropDownMenu />
) : (
  isKeycloakInitialized && <LoginButton />
}
```

#### HTTP requests

In certain places, the frontend sends an HTTP request, e.g. POST, to the backend. We use the function `authorizedFetch` in `Utils.jsx` (src\main\frontend\src\Utils.jsx), which configures the options for the JavaScript function `fetch`.
Since the app wasn't secured before, the part that actually puts the "authorized" in "authorizedFetch" was commented out. Now is the time to uncomment it:

``` 
headers: {
  // ----- keycloak w3: activate (uncomment) -----
  Authorization: `Bearer ${kc.token}`,
```

This adds the Authorization header to the HTTP request.
The header's value is the string "Bearer " followed by the actual token that the `keycloak` object currently holds.

#### Refreshing tokens

Any time we enter/refresh a page, our access token gets refreshed, i.e. replaced by a newer access token. But what if we are on the same page for longer than the lifetime of an access token (typically 10 minutes)? 
Well, we would want the token to be refreshed without having to manually refresh the page. 

We have defined the function `executeWithValidToken` for this purpose (also in `Utils.jsx`).
Several buttons already call this function as a wrapper around some other function (the "callback function"), but right now it does nothing besides executing that callback function:

``` 
export const executeWithValidToken = (callBackFunction) => {
  // ----- keycloak w3: comment out/remove -----
  callBackFunction();
  
  (...)
}
```

But we want this function to refresh the token if it is (almost) expired, before calling the callback function. 

Comment and uncomment as usual, to make it look like this:

```
export const executeWithValidToken = (callBackFunction) => {
  // ----- keycloak w3: comment out/remove -----
  // callBackFunction();
  
  // ----- keycloak w3: activate (uncomment) -----
  keycloak
    .updateToken()
    .then(() => callBackFunction())
    .catch(keycloak.login); // Couldn't do keycloak.updateToken -> There is no keycloak session, so log in.
};
```

### Integrate backend

In `pom.xml`, add the following dependency...
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-oauth2-resource-server</artifactId>
</dependency>
```

... and load the maven changes.

In the `application.properties` file (src\main\resources\application.properties), uncomment the last line by removing the `#`:
```
spring.security.oauth2.resourceserver.jwt.issuer-uri=http://localhost:8080/realms/workshop
```

Finally, uncomment (activate) all the code inside `SecurityConfig.java` (src\main\java\nl\first8\workshop\keycloak\config\SecurityConfig.java):

``` 
package nl.first8.workshop.keycloak.config;

// ----- keycloak w3: activate (uncomment) -----

(... the rest of the file)
```

This implements the logic for OAuth2 authentication against our Keycloak server, and contains a converter for our tokens.

## Testing

After all these changes we need to run the usual command `./mvnw spring-boot:run` in the root folder of the project, which packages the frontend files and puts them in a backend folder, and then compiles and runs the bundled backend+frontend.

Go back to [http://localhost:8000](http://localhost:8000) and the app should now have a login button:
![image](images/login-button.png)

* Click it, and you'll see the default Keycloak themed login screen:

![image](images/login-screen.png)

* Log in with the user you created in **w1-introduction**.
* Since Keycloak 24, a user is required to complete their profile when they first log in (it is possible to disable this default requirement.) Fill in the fields and press `Submit`:

![image](images/aap-profile.png)

Where the login button used to be, you'll now see your username, which you can click to open a dropdown menu with some buttons which will come in handy later on, and of course a logout button:

![image](images/dropdown.png)

Now our frontend and backend are secured in the sense that users need to be *authenticated* to do anything in the hotel app (other than creating a booking). 
In the next assignment, we'll add *authorization* checks on top of this.

**[Continue to w4 of the basic track](basic-w4-role-management.md)**