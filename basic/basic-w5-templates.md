# Templates

## Adding customization


Let's take the example of our account menu and see how to change its appearance. We'll just change the logo, which you can see at the top-left of any account page, for example at
 [http://localhost:8080/realms/workshop/account](http://localhost:8080/realms/workshop/account)

![image](images/logo-keycloak.png)

(You can also access this page by clicking `Keycloak Account` in the hotel app's dropdown menu.)

Let's try to replace that Keycloak logo by the First8 logo. For that, we need to add a new folder `mytheme` in the `themes` directory of our keycloak installation. 

We'll copy some content from the built-in templates so that we have all the required elements. These can be found within the theme directory of `../lib/lib/main/org.keycloak.keycloak-account-ui-2X.0.Z`.

* Unzip the jar somewhere.
* Go to the newly extracted `theme` folder.
* In the `keycloak.v3` folder, copy the `account` folder...

![image](images/account-folder.png)

* ...and paste it into the `themes/mytheme` folder we created earlier.

A First8 logo (first8.png) is located right here where the instructions you are currently reading are, in the `basic` folder.
Copy the logo into `mytheme/account/resources` .

Next, modify `account/theme.properties` by adding the line:

    logo=/first8.png

Finally, change the Account theme to the newly created mytheme:

* Restart keycloak (`bin/kc.sh start-dev` or `bin\kc.bat start-dev`)
* Go to the `workshop` realm. 
* Click on `Realm Settings` in the left-hand sidebar. 
* Then click on `Themes`. 
* From the dropdown menu next to `Account Theme`, choose `mytheme`. 
* Click the `Save` button.

And here's the top left of any account page (such as [the page from before](http://localhost:8080/realms/workshop/account)) looks now:

![image](images/logo-first8.png)

Similar to how we customized the account theme here, to change the look and feel of the other theme types, we need to add new folders called admin, email, or login, and follow the same process.

<br>

**You have finished the basic track of the workshop!**

If you are interested in the other tracks, check out the [README](../README.md), or jump straight to the start of another track (w0 and w1 can be skipped):

* [advanced-w2: Write and install plugins](../advanced/advanced-w2-write-and-install-plugins.md)
* [admin-w2: Third parties](../admin/admin-w2-third-parties.md)