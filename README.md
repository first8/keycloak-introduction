# Hi!
Welcome to the Keycloak workshop!

*If you have completely read the document you received per mail, you may skip this Readme and the next document (w0-installation), and instead __[continue with w1-introduction](w1-introduction.md)__*.

In this workshop you can choose your own adventure, whether you are a developer that wants to know the basics, or an 
administrator that is responsible for the setup within Keycloak, we got you covered!

We will shortly discuss the 3 tracks that you can choose from to start your workshop:

* **Basic**<br>
This track is meant for developers that need an introduction into securing their first application using Keycloak. 
We will discuss oauth2 terminology and go through some basic functionalities of Keycloak.

* **Advanced**<br>
This is for the developers that want to dive a bit deeper into Keycloak. This part discusses how to create and add your 
own plugins, and how to use the Keycloak API.

* **Admin**<br>
Lastly, the admin track is meant for the non-developers that are responsible for the configuration of Keycloak, for 
instance adding a third-party login and connecting an LDAP.

Of course you are always free to also follow exercises from a different track if you are interested in those subjects.

## CHOOSE YOUR OWN ADVENTURE!

Make sure you follow w0-installation and w1-introduction first so you have the basic setup ready for the other 
exercises. After that you are free to choose your path. Good luck!

**[Start the workshop](w0-installation.md)**